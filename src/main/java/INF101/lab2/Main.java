package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Pikachu");
        pokemon2 = new Pokemon("Oddish");

        while (pokemon1.isAlive() && pokemon2.isAlive()){
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive()){
                pokemon2.attack(pokemon1);
            }
            else{
                break;
            }
        }

        // så lenge ingen pokemon er død:
        //  pokemon1 angriper pokemon2
        //  hvis pokemon2 er i live: 
                //pokemon2 angriper pokemon1
    }
}
